package com.howtodoinjava.demo.web;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.howtodoinjava.demo.exception.RecordNotFoundException;
import com.howtodoinjava.demo.model.Customer;
import com.howtodoinjava.demo.repository.CustomerRepository;

@Controller
@RequestMapping("/")
public class CustomerMvcController 
{
	@Autowired
	CustomerRepository repos;

	@RequestMapping
	public String getAllCustomer(Model model) 
	{
		List<Customer> list = repos.findAll();

		model.addAttribute("customers", list);
		return "list-customers";
	}

	@RequestMapping(path = {"/edit", "/edit/{id}"})
	public String editCustomerById(Model model, @PathVariable("id") Optional<Long> id) 
							throws RecordNotFoundException 
	{
		if (id.isPresent()) {
			Customer entity = repos.findById(id.get()).orElseThrow();
			model.addAttribute("customer", entity);
		} else {
			model.addAttribute("customer", new Customer());
		}
		return "add-edit-customer";
	}
	
	@RequestMapping(path = "/delete/{id}")
	public String deleteCustomerById(Model model, @PathVariable("id") Long id) 
							throws RecordNotFoundException 
	{
		repos.deleteById(id);
		return "redirect:/";
	}

	@RequestMapping(path = "/createCustomer", method = RequestMethod.POST)
	public String createOrUpdateCustomer(Customer customer) 
	{
		repos.save(customer);
		return "redirect:/";
	}
}
